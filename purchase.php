<?php
include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-gift"></i> Store</h2>
                        <p class="text-center">
                            <?php
                            if (isset($_SESSION['id']))
                            {
                                $bnetID = $_SESSION['id'];
                                $itemID = stripslashes(mysqli_real_escape_string($mysqliW, $_POST['itemID']));
                                $ProductID = stripslashes(mysqli_real_escape_string($mysqliW, $_POST['productID']));
                                $playerID = stripslashes(mysqli_real_escape_string($mysqliC, $_POST['characterID']));

                                //let's check if user have enough coins
                                //let's get user coins
                                $acc_query = $mysqliA->query("SELECT * FROM `account` WHERE `battlenet_account` = '$bnetID';") or die (mysqli_errno($mysqliA));
                                while($acc_res = $acc_query->fetch_assoc())
                                {
                                    //user data
                                    $accountID = $acc_res['id'];
                                    $accountCoins = $acc_res['coins'];
                                }

                                //now lets get product price
                                $prod_query = $mysqliA->query("SELECT * FROM `store_items` WHERE `id` = '$ProductID'") or die (mysqli_error($mysqliA));
                                while($prod_res = $prod_query->fetch_assoc())
                                {
                                    $productPrice = $prod_res['price'];
                                }


                                //player account coints vs product price
                                if($accountCoins < $productPrice)
                                {
                                    echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> Purchase error! <br /> You don\'t have enough <span class="badge badge-warning"><i class="fad fa-coin"></i></span> (coins).
                                        </div>
                                        <center><a href="/buy-coins.php" class="btn btn-outline-warning">Buy more <i class="fad fa-coin"></i> </a></center>
                                    ';
                                }
                                else
                                {
                                    $updateCoin = ($accountCoins-$productPrice);
                                    $update_acc = $mysqliA->query("UPDATE `account` SET `coins` = '$updateCoin' WHERE `id` = '$accountID';") or die (mysqli_error($mysqliA));
                                    if($update_acc === true)
                                    {
                                        //let's insert the item to player mail
                                        $blizz_mail = $mysqliC->query("INSERT INTO `mail`(`messageType`, `stationery`, `mailTemplateId`, `sender`, `receiver`, `subject`, `body`, `has_items`, `money`, `cod`, `checked`) VALUES ( 0, 61, 0, '$playerID', '$playerID', 'Your Order', 'Thank you for your purchase. Your item is attached to this email!', 1, 0, 0, 0);") or die (mysqli_error($mysqliC));
                                        if($blizz_mail === true)
                                        {
                                            //add the item to mail
                                            $item_mail = $mysqliC->query("SELECT * FROM `mail` WHERE `stationery` = '61' AND `receiver` = '$playerID' ORDER BY `id` DESC LIMIT 1;") or die (mysqli_error($mysqliC));
                                            while($mail_res = $item_mail->fetch_assoc())
                                            {
                                                $mailID = $mail_res['id'];
                                                //let's create item
                                                $create_item = $mysqliC->query("INSERT INTO `item_instance`(`itemEntry`, `owner_guid`, `creatorGuid`, `giftCreatorGuid`, `count`, `duration`, `charges`, `flags`, `enchantments`, `randomBonusListId`, `durability`, `playedTime`, `text`, `transmogrification`, `enchantIllusion`, `battlePetSpeciesId`, `battlePetBreedData`, `battlePetLevel`, `battlePetDisplayId`, `context`, `bonusListIDs`) VALUES ( '$itemID', '$playerID', 0, 0, 1, 0, '-1 0', 0, '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ', 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, '');") or die (mysqli_error($mysqliC));
                                                //let's get the item guid..
                                                $item_guid_query = $mysqliC->query("SELECT * FROM `item_instance` WHERE `itemEntry` = '$itemID' AND `owner_guid` = '$playerID' ORDER BY `guid` DESC LIMIT 1;") or die (mysqli_error($mysqliC));
                                                while($item_res = $item_guid_query->fetch_assoc())
                                                {
                                                    $itemGUID = $item_res['guid'];
                                                }
                                                //let's add the item to the mail
                                                $add_item_mail = $mysqliC->query("INSERT INTO `mail_items`(`mail_id`, `item_guid`, `receiver`) VALUES ('$mailID', '$itemGUID', '$playerID');") or die (mysqli_error($mysqliC));
                                                if($add_item_mail === true)
                                                {
                                                    //update accounts coins (not less than 0 even it shouldn't happen

                                                    echo '
                                                        <div class="alert alert-success" role="alert">
                                                          <i class="fad fa-check-circle"></i> Purchase complete! <br /> Login on your character and check the email.
                                                        </div>
                                                    ';
                                                    header('refresh:5; url=/ucp.php');
                                                }
                                                else
                                                {
                                                    echo '
                                                        <div class="alert alert-danger" role="alert">
                                                          <i class="fad fa-exclamation-circle"></i> Purchase error! (ERR_MAIL_ITEM_ADD)
                                                        </div>
                                                    ';
                                                    header('refresh:5; url=/store.php');
                                                }
                                            }
                                        }
                                        else
                                        {
                                            echo '
                                                <div class="alert alert-danger" role="alert">
                                                  <i class="fad fa-exclamation-circle"></i> Purchase error! (ERR_MAIL_CREATE)
                                                </div>
                                            ';
                                            header('refresh:5; url=/store.php');
                                        }
                                    }
                                    else
                                    {
                                        echo '
                                        <div class="alert alert-danger" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> Purchase error! (ERR_COIN_UPDATE)
                                        </div>
                                        ';
                                        header('refresh:5; url=/store.php');
                                    }
                                }
                            }
                            else
                            {
                                echo '
                                    <div class="alert alert-info" role="alert">
                                      <i class="fad fa-exclamation-circle"></i> You need to be <a href="/login.php">logged in</a> to access this page!
                                    </div>
                                ';
                            }
                            ?>
                            <br/>
                        </p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>