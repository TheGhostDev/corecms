<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-user-plus"></i> Register!</h2>
                        <p class="card-text">
                            If you want to join us please complete the form below!
                        </p>
                        <p class="text-center">
                            <?php
                            if (isset($_POST['register']))
                            {
                                //do insert
                                $username = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['username']));
                                $password = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['password']));
                                $email = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['email']));

                                $acc_paas = sha1(strtoupper($username).':'.strtoupper($password));
								$acc_pass_upper = strtoupper($acc_paas);
                                $bnet_pass = bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password)))))));
								$bnet_upper_pass = strtoupper($bnet_pass);

                                $query = $mysqliA->query("SELECT * FROM `battlenet_accounts` WHERE `email` = '$email';") or die (mysqli_error($mysqliA));
                                $userquery = $mysqliA->query("SELECT * FROM `web_user` WHERE `username` = '$username';") or die (mysqli_error($mysqliA));
                                $count = $query->num_rows;
                                $usercount = $userquery->num_rows;
                                if ($count > 0 or $usercount > 0)
                                {
                                    echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> There is already an account with this username or this email!
                                        </div>
                                    ';
                                    header("refresh:3; url=$custdir/register.php");
                                }
                                else
                                {
                                    //insert
                                    $bnet_create = $mysqliA->query("INSERT INTO `battlenet_accounts` (email, sha_pass_hash) VALUES ('$email', '$bnet_upper_pass');") or die (mysqli_error($mysqliA));
                                    if($bnet_create === true)
                                    {
                                        //let's get the new battlenet_account created and add it to the account table :)
                                        $bnet_get = $mysqliA->query("SELECT * FROM `battlenet_accounts` WHERE `email` = '$email';") or die (mysqli_error($mysqliA));
                                        while($res_bnet = $bnet_get->fetch_assoc())
                                        {
                                            $bnet_id = $res_bnet['id'];
                                            //let's insert
                                            $max_id = $mysqliA->query("SELECT MAX(id) AS id FROM account;") or die(mysqli_error($mysqliA));
                                            $res_maxid = $max_id->fetch_assoc();
                                            $new_id = $res_maxid['id'] + 1;
                                            $user_id = "".$new_id. "#1";

                                            $acc_create = $mysqliA->query("INSERT INTO `account` (`username`, `sha_pass_hash`, `email`, `battlenet_account`, `battlenet_index`, `reg_mail`) VALUES ('$user_id', '$acc_pass_upper', '$email', '$bnet_id', '1', '$email');") or die (mysqli_error($mysqliA));
                                            $web_user = $mysqliA->query("INSERT INTO `web_user` (`id`, `username`) VALUES ('$bnet_id', '$username');") or die (mysqli_error($mysqliA));
                                            echo '
                                                <div class="alert alert-success" role="alert">
                                                  <i class="fad fa-check-circle"></i> Your account was created! Now you can login!
                                                </div>
                                            ';
                                            header("refresh:3; url=$custdir/login.php");
                                        }
                                    }
                                }
                            }
                            else
                            {
                            ?>
								<form name="register" method="post" action="" enctype="multipart/form-data">
									<div class="form-group">
										<input type="text" class="form-control" name="username" placeholder="Username" required>
									</div>
									<div class="form-group">
										<input type="email" class="form-control" name="email" placeholder="Your email address" required>
									</div>
									<div class="form-group">
										<input type="password" class="form-control" name="password" placeholder="Password" required>
									</div>
									<button type="submit" name="register" class="btn btn-warning form-control"><i class="fad fa-user-plus"></i> Create account</button>
								</form>
							<?php
							}
							 ?>
                        <br/>
                        <div class="alert alert-info">
                            <i class="fad fa-exclamation-circle"></i> If you create an account you agree with our terms and conditions!
                        </div>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>