<?php /** @noinspection HtmlUnknownTarget */
include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-house-user"></i> User Control Panel (UCP)</h2>
                        <p class="text-center">
                            <?php
                            if (isset($_SESSION['id']))
                            {
                                ?>
                                <div class="alert alert-success" role="alert">
                                  <i class="fad fa-check-circle"></i> Welcome to your user control panel, here you can see details about your account!
                                </div>
                                <nav>
                                  <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link wow active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fad fa-file-user"></i> Account Details</a>
                                    <a class="nav-item nav-link wow" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fad fa-users"></i> My characters</a>
                                    <a class="nav-item nav-link wow" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="fad fa-shopping-cart"></i> My orders</a>
                                  </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                      <?php include('inc/my_account.php'); ?>
                                  </div>
                                  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                  <?php include('inc/my_charcters.php'); ?>
                                  </div>
                                  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                  <?php include('inc/my_orders.php'); ?>
                                  </div>
                                </div>
                            <?php
                            }
                            else
                            {
                                echo '
                                        <div class="alert alert-warning" role="alert">
                                          <i class="fad fa-exclamation-circle"></i> You need to be <a href="/login">logged in</a> to access this page!
                                        </div>
                                    ';
                            }
                            ?>
                            <br/>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <i class="fad fa-clock"></i> Last update: <span class="badge badge-info">LIVE</span>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>